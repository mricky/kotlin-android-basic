package basic.kotlin.kotlinbasic

import android.graphics.Color
import android.media.Rating
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.text.TextWatcher;
import android.view.View
import android.widget.*
import android.app.ProgressDialog;
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // init menggunakan syntetic
        tv_hello_world.setText("hello ricky");

        val edit = findViewById<EditText>(R.id.et_hello) as EditText;
        // Button
        val button = findViewById<Button>(R.id.button) as Button;
        val button1 = findViewById<Button>(R.id.button1) as Button;

        button1.setOnClickListener{
            Toast.makeText(this@MainActivity,"By Using Java",Toast.LENGTH_SHORT).show();
        }
        // Radio Button
        val background = findViewById<LinearLayout>(R.id.background) as LinearLayout;
        val radio = findViewById<RadioGroup>(R.id.radio) as RadioGroup;

        radio.setOnCheckedChangeListener{radio,checkedid->
            when(checkedid){
                R.id.red->{
                    background.setBackgroundColor(Color.parseColor("#FF0000"));
                }
                R.id.green->{
                    background.setBackgroundColor(Color.parseColor("#00FF00"))
                }
                R.id.blue->{
                    background.setBackgroundColor(Color.parseColor("#0000FF"))
                }
            }
        }
        // checkbox
        val btn_checkbox = findViewById<Button>(R.id.btnCheckbox);
        val cbk_kotlin = findViewById<CheckBox>(R.id.cbk_kotlin) as CheckBox;
        val cbk_java = findViewById<CheckBox>(R.id.cbk_java) as CheckBox;

        btn_checkbox.setOnClickListener{
            if(cbk_kotlin.isChecked == true && cbk_java.isChecked == true){
                Toast.makeText(this@MainActivity,"Kotlin + Java",Toast.LENGTH_SHORT).show();

            }
            else if(cbk_java.isChecked == true){
                Toast.makeText(this@MainActivity, "Java",Toast.LENGTH_SHORT).show();
            }
            else if(cbk_kotlin.isChecked == true)
            {
                Toast.makeText(this@MainActivity,"Kotlin",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this@MainActivity,"Nothing Check",Toast.LENGTH_SHORT).show();
            }

        }
        // Rating Bar
        val btnRatingBar = findViewById<Button>(R.id.btnRatingBar) as Button;
        val ratingBar = findViewById<RatingBar>(R.id.ratingBar) as RatingBar;

        btnRatingBar.setOnClickListener {
            val ratingBarValue = ratingBar.rating;

            Toast.makeText(this@MainActivity, "Rating :"+ratingBarValue,Toast.LENGTH_SHORT).show();
        }
        // Spinner
        val spinnerId = findViewById<Spinner>(R.id.spinnerId) as Spinner;
        val fruit = arrayOf("Aple","Jeruk","Lime","Grapes");
        val arrayAdr = ArrayAdapter(this@MainActivity,android.R.layout.simple_spinner_dropdown_item,fruit);
        spinnerId.adapter = arrayAdr;

        val switchId = findViewById<Switch>(R.id.switchId) as Switch;
        switchId.setOnClickListener {
            if(switchId.isChecked){
                Toast.makeText(this@MainActivity,"Switch On",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this@MainActivity,"Switch Off",Toast.LENGTH_SHORT).show();
            }
        }
        // Progress Dialog
        val prgDialog = ProgressDialog(this@MainActivity);
        val btnPDialog = findViewById<Button>(R.id.buttonProgress) as Button;

        btnPDialog.setOnClickListener {
            prgDialog.setTitle("Progress Dialog");
            prgDialog.setMessage("This progress Dialog Box");
            prgDialog.show();
        }
        // auto complete skip


    }
    fun buttonFunc(v: View){
        if(v.id == R.id.button){
            Toast.makeText(this@MainActivity, "By xml",Toast.LENGTH_SHORT).show();
        }
    }
    fun MainActivity.a(){
        tv_hello_world.setText("Hello Ricky A");
    }
    fun MainActivity.b(){
        tv_hello_world.setText("Hello Ricky B");
    }
}
